<nav id="categories" class="navbar navbar-expand-lg p-0 categories">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-categories" aria-controls="navbar-categories" aria-expanded="false" aria-label="Toggle navigation">
		@lang('website.All Categories')
  </button>
  
  <div class="collapse navbar-collapse" id="navbar-categories">

    <ul class="navbar-nav flex-column">
    @foreach($result['commonContent']['categories'] as $categories_data)
      @if($categories_data->total_products > 0)
      <li class="nav-item dropdown">
        <a href="{{ URL::to('/shop')}}?category={{$categories_data->slug}}" class="nav-link dropdown-toggle">
          <img class="img-fuild" src="{{asset('').$categories_data->icon}}">
          {{$categories_data->name}} &nbsp;&nbsp;&nbsp; | &nbsp;{{$categories_data->total_products}}
          @if(count($categories_data->sub_categories)>0) 
            <i class="fa fa-angle-right " aria-hidden="true"></i> 
          @endif
        </a>
        
        @if(count($categories_data->sub_categories)>0)
        <ul class="dropdown-menu multi-level">
        	@foreach($categories_data->sub_categories as $sub_categories_data)  
            @if($sub_categories_data->total_products > 0)          
            <li class="dropdown-submenu">
              <a  class="dropdown-item" tabindex="-1" href="{{ URL::to('/shop')}}?category={{$sub_categories_data->sub_slug}}">
                <img class="img-fuild" src="{{asset('').$sub_categories_data->sub_icon}}">
                {{$sub_categories_data->sub_name}} &nbsp;&nbsp;&nbsp; | &nbsp; {{$sub_categories_data->total_products}}
              </a>              
            </li>
            @endif            
          @endforeach 
        </ul>
        @endif
      </li>
      @endif
    @endforeach
    </ul>
  </div>
</nav>


